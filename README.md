# Des nombres et des mots

Les tests d'un projet visent 3 objectifs :

* La documentation fonctionnelle du projet
* La non régression
* Faire émerger le code

Nous allons nous concentrer sur l'émergence du code

# Rappel des étapes du TDD

* *Test* :
    * Lancer le chronomètre
    * Écrire un test
    * Vérifier qu'il est rouge (Tout compile sans erreur mais le test ne passe pas)
    * Commiter avec en message `Test : intention fonctionnel et intention de code`
    * Arrêter le chronomètre
* *Implémentation* :
    * Lancer le chronomètre
    * Faire passer le test au vert, et bien évidement, conserver tous les précédents au verts aussi.
    * Commiter avec en message `Impl : intention fonctionnel`
    * Arrêter le chronomètre
* *Restructuration* :
    * Lancer le chronomètre
    * Restructurer le code
    * Et bien évidement, conserver tous les tests verts
    * Commiter avec en message `Refacto : description des éléments changés`
    * Arrêter le chronomètre

# Convertir des nombres en mots

Pour ce dernier TP, vous allez faire du TDD seul.

L'objectif est de réaliser un convertisseur de nombres en lettres.

Vous allez implémenter la classe suivante :
``` java
public class Numeral {
    public Numeral (String number) {
        ...
    }
    public String toLetters () {
        ...
    }
}
```

## Initialisation

Initialisez le projet maven avec le bon nom, les bons dossiers et importez-le dans votre IDE préféré.

### Barème de relecture

Dans toute la suite attention de bien suivre la boucle TDD et de faire des commits à la fin de chaque étape, et des push à la fin de chaque boucle. Un git log du projet devrait ressembler à ça :
```
...
b28a944 Refactor tests
b28a944 Impl addition
8ab9f43 Add second test for addition
efd6601 Green bar for first addition
2530399 Add first test for addition
f9aa3f3 Bootstrap project
```

Vous apporterez une attention toute particulière à la concordance entre les messages de commits, les heures de commits et les contenues des commits.

Vous apporterez une attention toute particulière à la qualité des tests.

Rappel de règles de qualité d'un bon test : 

* Il est rapide (quelques secondes max)
* Je peux le jouer 2 fois de suite il est encore vraie, même à plusieurs jours d'intervalles.·
* Il est explicite, les destinataires de ce test peuvent le lire et le comprendre.
* Il ne test qu'une règle.
* Il est indépendant des autres tests :·
    * je peux jouer les tests dans le désordre
    * je peux jouer tous les tests en même temps.
* Je peux restructurer mon code sans avoir à le changer

Pour finir, il va sans dire que l'objectif est d'aller le plus loin possible dans le sujet.

## Les Unités

En initialisant notre objet Numeral avec une string d'un chiffre, il est possible d'obtenir la version écrite en lettres de ce chiffre commençant par une majuscule.
Par exemple :
| Entrée      | Sortie |
|-------------|--------|
| 1           | Un     |
| 7           | Sept   |

## Première dizaine
En initialisant notre objet Numeral avec une string de deux chiffres, il est possible d'obtenir la version écrite en lettres de ce chiffre commençant par une majuscule.
Par exemple :
| Entrée      | Sortie |
|-------------|--------|
| 10          | Dix    |
| 18          | Dix-huit |

## Seconde dizaine
En initialisant notre objet Numeral avec une string de deux chiffres, il est possible d'obtenir la version écrite en lettres de ce chiffre commençant par une majuscule.
Par exemple :
| Entrée      | Sortie |
|-------------|--------|
| 20          | Vingt    |
| 21          | Vingt et un |

## Autres dizaines
En initialisant notre objet Numeral avec une string de deux chiffres, il est possible d'obtenir la version écrite en lettres de ce chiffre commençant par une majuscule.
Par exemple :
| Entrée      | Sortie |
|-------------|--------|
| 39          | Trente-neuf        |
| 96          | Quatre-vingt-seize |

## Centaines
En initialisant notre objet Numeral avec une string de trois chiffres, il est possible d'obtenir la version écrite en lettres de ce chiffre commençant par une majuscule.
Par exemple :
| Entrée      | Sortie |
|-------------|--------|
| 100         | Cent               |
| 200         | Deux cents               |
| 375         | Trois cent soixante-quinze |

## Des euros
En initialisant notre objet Numeral avec une string décrivant un prix en euro, il est possible d'obtenir la version écrite en lettres de ce prix commençant par une majuscule.
En Français, le symbole monétaire se place après tous les chiffres séparés par une espace.
Par exemple :
| Entrée      | Sortie |
|-------------|--------|
| 1 €         | Un euro              |
| 845 €       | Huit cent quarante-cinq euros |

## Des milles

En initialisant notre objet Numeral avec une string, il est possible d'obtenir la version écrite en lettres de ce chiffre ou du prix commençant par une majuscule.
Par exemple :
| Entrée      | Sortie |
|-------------|--------|
| 1000        | Mille              |
| 4000        | Quatre mille             |
| 5668 €      | Cinq mille six cents soixante-huit euros |

# Plus d'unités
* Ajouter la gestion d'autres monnaie comme le dollar, la livre ou le yen.
* Ajouter la gestion des unités métriques (km, m, cm, ...)

# Des nombres de plus en plus grand
* Ajouter la gestion des nombres jusqu'au milliard.
* Ajouter la gestion des quintillions.

# Affichage en chiffres

Ajouter la méthode `public String toFigures()` qui retourne le nombre formaté en respectant les règles typographique françaises (regroupement des chiffres par 3 séparés par des espaces).

# L'opération inverse

Ajouter la méthode `public Numeral fromLetters(String number)` qui permet de parser un nombre écrit en lettre.

# Barème de correction

Comme une dictée, la copie démarre avec 20 points. Chaque erreur enlève des points.
Pour garder les 20 points, le programme doit au moins savoir convertire jusqu'à 999. À partir de la gestion des euros, ce sont donc des questions bonus.

L'ensemble du code doit respecter des règles de clean code (SOLID, DEMETER, lisibilité, DRY, KISS, YAGNI, etc.) en fonction de ces règles, estimez la note du code que vous relisez.

* Un test rouge : -1 point
* Une règle de clean code non respectée : -1 point
* Une règle de qualité de test non respectée : -1 point

Je vous invite à ajouter vos propres tests pour vérifier que le code respecte bien le besoin métier. Attention le but est bien d'évaluer la qualité du code et pas sa complétude en rapport à toutes les questions. Si vous avez des tests correspondant aux questions traitées par les étudiants que vous corrigez qui ne passent pas vous pouvez enlever des points.

Pour vérifier que la boucle TDD a bien été respectée, vous avez 2 outils à votre disposition :

* `git log --oneline --decorate --graph --all --format="%h %Cblue%ad% %C(auto) %s"` qui permet de lister tous les commits avec l'heure et le commentaire
* `for commit in $(git log --reverse --pretty=format:%H); do git checkout $commit; git show --stat $commit; read; done` qui rejoue la liste de tous les commit 1 à 1 en affichant quelques éléments. Pour passer d'un commit à l'autre, il faut sortir du git show et valider avec la touche entrée.
* Pour revenir au dernier commit `git checkout master`

À chaque non respect de la boucle TDD -1 point s'impose.

