public class Numeral {
    String number;
    public Numeral (String number) {
        this.number=number;
    }

    public String convertirChiffre(char chiffre){
        String res="";
        switch (chiffre) {
            case '1':
                res= "un";
                break;
                case '2':
                res="deux";
                break;
                case '3':
                res="trois";
                break;
                case '4':
                res="quatre";
                break;
                case '5':
                res="cinq";
                break;
                case '6':
                res="six";
                break;
                case '7':
                res="sept";
                break;
                case '8':
                res="huit";
                break;
                case '9':
                res="neuf";
                break;
                case '0':
                res="zero";
                break;
            default:
                res="erreur produite chiffre";
                break;
        }
        return res;
    }

    public String toLettersNumber(){
        String res="";
        if(this.number.length()==2){
            return traitementDizaine(this.number);
        }
        if (this.number.length()==3) {
            return traitementCentaine(this.number);
        }
        return res;    
    }

    private String traitementCentaine(String centaine) {
        String res="";
        if (centaine.charAt(1)=='0'&&centaine.charAt(2)=='0') {
            
        }
        return res;
    }

    public String traitementDizaine(String dizaine){
        String res="";
        switch (dizaine.charAt(0)) {
            case '1':
                res= traitementPremiereDizaine(dizaine.charAt(1));
                break;
                case '2':
                res= ecritureDizaine("vingt",dizaine.charAt(1));
                break;
            case '3':
                res=ecritureDizaine("trente", dizaine.charAt(1));
                break;
                case '4':
                res=ecritureDizaine("quarante", dizaine.charAt(1));
                break;
                case '5':
                res=ecritureDizaine("cinquante", dizaine.charAt(1));
                break;
                case '6':
                res=ecritureDizaine("soixante", dizaine.charAt(1));
                break;
                case '7':
                res="soixante-"+ traitementPremiereDizaine(dizaine.charAt(1));
                break;
                case '8':
                res=ecritureDizaine("quatre-vingt", dizaine.charAt(1));
                break;
                case '9':
                res="quatre-vingt-"+traitementPremiereDizaine(dizaine.charAt(1));
                break;
            default:                
            res= "erreur , caracter non reconnu";
                break;
        }

        return res;
    }

    public String traitementPremiereDizaine(char dizaine){
        String res="";
        switch (dizaine) {
            case '1':
                res="onze";
                break;
            case '2':
            res="douze";
            break;
            case '3':
            res="treize";
            break;
            case '4':
            res="quatorze";
            break;
            case '5':
            res="quinze";
            break;
            case '6':
            res="seize";
            case '0':
            res="dix";
            break; 
            default:
            res="dix-"+this.convertirChiffre(dizaine);
                break;
        }

        return res;
    }


    private String ecritureDizaine(String prefixe,char chiffre) {
        String res=prefixe;
        switch (chiffre) {
            case '0':
            if(prefixe.substring(prefixe.length()-5).equals("vingt")){
                res+='s';
            };
                break;
            case '1':
                res+= " et un";
                break;
            case '2':
            res+= "-deux";
            break;
            case '3':
            res+= "-trois";
            break;
            case '4':
            res+= "-quatre";
            break;
            case '5':
            res+= "-cinq";
            break;
            case '6':
            res+= "-six";
            break;
            case '7':
            res+= "-sept";
            break;
            case '8':
            res+= "-huit";
            break;
            case '9':
            res+= "-neuf";
            break;
            default:
            res+= " erreur ,ce caracter n'est pas un chiffre";
                break;
        }    
        return res;
    }

    public String toLetters () {
        String res="";
        System.out.println(this.number.length());;
        if(this.number.length()>1){     
            res = this.toLettersNumber();
        }else{
            res= this.convertirChiffre(number.charAt(0));
        }
        res=res.substring(0,1).toUpperCase()+res.substring(1);
        return res;
    }
}
