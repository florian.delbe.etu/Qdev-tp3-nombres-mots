import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestNumeral {
    @Test
    public void testConversionChiffre(){
        Numeral num= new Numeral("1");
        assertEquals("Un",num.toLetters());
      }

    @Test
    public void testConversionPremièreDizaine(){
        Numeral num= new Numeral("11");
        assertEquals("Onze", num.toLetters());
        Numeral second=new Numeral("18");
        assertEquals("Dix-huit", second.toLetters());
        Numeral dix=new Numeral("10");
        assertEquals("Dix", dix.toLetters());
    }

    @Test
    public void testConversionSecondeDizaine(){
        Numeral num= new Numeral("21");
        assertEquals("Vingt et un", num.toLetters());
        Numeral second=new Numeral("28");
        assertEquals("Vingt-huit", second.toLetters());
        Numeral dix=new Numeral("20");
        assertEquals("Vingt", dix.toLetters());
    }

    @Test
    public void testConversionToutesDizaine(){
        Numeral num= new Numeral("41");
        assertEquals("Quarante et un", num.toLetters());
        Numeral second=new Numeral("38");
        assertEquals("Trente-huit", second.toLetters());
        Numeral dix=new Numeral("90");
        assertEquals("Quatre-vingt-dix", dix.toLetters());
    }

    @Test
    public void testConversionCentaine(){
        Numeral num= new Numeral("100");
        assertEquals("Cent", num.toLetters());
        Numeral second=new Numeral("200");
        assertEquals("Deux cents", second.toLetters());
        Numeral trois=new Numeral("375");
        assertEquals("Trois cent soixante-quinze", trois.toLetters());

    }
}
